#!/usr/bin/python3

import pydmm.pydmm as pd
from time import time, sleep
from relay import Relay
from datetime import datetime
import os
import signal
import sys
import threading


USBDEV = "/dev/ttyUSB0"
RELAYVENDOR = 0x16c0
RELAYPRODUCT = 0x05df
REDUCED_POWER = 0.45
STARTTEMP = 80
RAMPENDTEMP = 150
ENDTEMP = 225


def readTemp():
    try:
        temp = pd.read_dmm(port=USBDEV, timeout=3)
    except KeyError:
        temp = pd.read_dmm(port=USBDEV, timeout=3)
    return temp


def keyboardInterruptHandler(ignore, ignore2):
    global RELAY
    RELAY.state(1, on=False)
    sys.exit()


def pwm():
    global PWM_STATE, PWM_RUNNING, RELAY
    while PWM_RUNNING:
        RELAY.state(1, on=True)
        sleep(PWM_STATE)
        RELAY.state(1, on=False)
        sleep(1 - PWM_STATE)


def printStatus(targetTemp, currentTemp, elapsed, log, message=""):
    global PWM_STATE
    os.system('clear')
    if currentTemp < STARTTEMP:
        print("Preheating...")
    print("Target temp: %0.2f °C" % targetTemp)
    print("Current temp: %0.2f °C" % currentTemp)
    print("Elapsed: %0.2f s" % elapsed)
    print("PWM: %0.2f%%" % (PWM_STATE * 100))
    if message != "":
        print("")
        print(message)
    log.write("%0.2f;%0.2f\n" % (time() - GLOBAL_START, currentTemp))
    log.flush()


### MAIN PROGRAM #######################################################################################################

# noinspection PyBroadException
try:

    signal.signal(signal.SIGINT, keyboardInterruptHandler)

    # initialize log file
    GLOBAL_START = time()
    dt = datetime.now().isoformat()
    log = open(dt.replace(":", "_") + ".csv", "w")
    log.write("Seconds;Temp °C\n")

    # initialize relay
    RELAY = Relay(idVendor=RELAYVENDOR, idProduct=RELAYPRODUCT)
    RELAY.state(1, on=False)

    # start pwm thread
    PWM_STATE = 1.0
    PWM_RUNNING = True
    thread = threading.Thread(target=pwm)
    thread.start()
    starttime = time()

    # heat to start temp
    temp = 0
    while temp < STARTTEMP:
        temp = readTemp()
        printStatus(STARTTEMP, temp, time() - starttime, log)

    # ramp up to 150 degrees, reduced power
    PWM_STATE = REDUCED_POWER
    while temp < RAMPENDTEMP:
        temp = readTemp()
        printStatus(RAMPENDTEMP, temp, time() - starttime, log)

    # full power until end temp is reached
    PWM_STATE = 1.0
    while temp < ENDTEMP:
        temp = readTemp()
        printStatus(ENDTEMP, temp, time() - starttime, log)

    PWM_RUNNING = False
    PWM_STATE = 0.0
    RELAY.state(1, on=False)

    sleeptime = time()
    while (time() - sleeptime) < 10:
        sleep(1)
        temp = readTemp()
        printStatus(ENDTEMP, temp, time() - starttime, log, "Baking finished, wait for opening...")
    os.system("sudo -u jh mplayer beep.wav &")
    print("\n\nOpen the oven!")

    # continue logging for 100s
    st = time()
    while (time() - st) < 100:
        temp = readTemp()
        printStatus(25, temp, time() - starttime, log)

    log.close()

    os.system("chown jh.jh *.csv")

except KeyboardInterrupt:
    RELAY.state(1, on=False)
    sys.exit(0)
